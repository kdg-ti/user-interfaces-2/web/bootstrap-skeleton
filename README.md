# About

## Usage

- From the project directory enter the following commands:

```
$ npm i
$ npm start
```

## Technologies used

- eslint
- webpack
  - loaders for css, fonts, etc.
- npm
