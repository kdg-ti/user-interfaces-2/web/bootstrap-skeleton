const HtmlWebpackPlugin = require('html-webpack-plugin')
const path = require('path')
const dist = path.resolve(__dirname, 'dist')

module.exports = {
  mode: 'development',
  devtool: 'source-map',
  plugins: [new HtmlWebpackPlugin({ template: './src/html/index.html' })],
  module: {
    rules: [
      {
        test: /\.css$/,
        use: ['style-loader', 'css-loader']
      },
      {
        test: /\.(eot|woff|woff2|ttf|svg)(\?\S*)?$/,
        use: ['file-loader']
      }
    ]
  },
  devServer: {
    contentBase: dist,
    open: true,
    overlay: true
  }
}
